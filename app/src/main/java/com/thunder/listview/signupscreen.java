package com.thunder.listview;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class signupscreen extends AppCompatActivity {

    //View Elements
    TextInputLayout mFirstNametil;
    TextInputLayout mLastNametil;
    TextInputLayout mEmailtil;
    TextInputLayout mPasswordtil;
    TextInputLayout mConfirmpasstil;
    TextInputEditText mFirstNametie;
    TextInputEditText mLastNametie;
    TextInputEditText mEmailtie;
    TextInputEditText mPasswordtie;
    TextInputEditText mConfirmpasstie;
    Button mSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signupscreen);

        findViews();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("REGISTRATION");
        getSupportActionBar().setSubtitle("CREATE YOUR ACCOUNT !");


        mSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean errorState = false;


                if (mEmailtie.getText().toString().equals("")) {
                    errorState = true;
                    mEmailtil.setErrorEnabled(true);
                    mEmailtil.setError("Please enter email");
                    //Toast.makeText(MainActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(mEmailtie.getText().toString()).matches()) {
                    //Toast.makeText(MainActivity.this, "Please enter Valid Email", Toast.LENGTH_SHORT).show();
                    errorState = true;
                    mEmailtil.setErrorEnabled(true);
                    mEmailtil.setError("Please enter Valid Email");
                } else {
                    mEmailtil.setError(null);
                    mEmailtil.setErrorEnabled(false);
                }

                if (mFirstNametie.getText().toString().equals("")) {
                    errorState = true;
                    mFirstNametil.setErrorEnabled(true);
                    mFirstNametil.setError("Please enter some name !");
                } else {
                    mFirstNametil.setError(null);
                    mFirstNametil.setErrorEnabled(false);

                }
                if (mLastNametie.getText().toString().equals("")) {
                    //  Toast.makeText(MainActivity.this, "Please enter Password", Toast.LENGTH_SHORT).show();
                    errorState = true;
                    mLastNametil.setErrorEnabled(true);
                    mLastNametil.setError("*Enter some name");
                } else {
                    mLastNametil.setError(null);
                    mLastNametil.setErrorEnabled(false);
                }
                if (mPasswordtie.getText().toString().equals("")) {
                    //  Toast.makeText(MainActivity.this, "Please enter Password", Toast.LENGTH_SHORT).show();
                    errorState = true;
                    mPasswordtil.setErrorEnabled(true);
                    mPasswordtil.setError("*password is required");
                } else if (mPasswordtie.getText().toString().length() < 6) {
                    errorState = true;
                    mPasswordtil.setErrorEnabled(true);
                    mPasswordtil.setError("Password cant be small than 6 charactors");
                    //Toast.makeText(MainActivity.this, "Password cant be small than 6 charactors", Toast.LENGTH_SHORT).show();
                } else {
                    mPasswordtil.setError(null);
                    mPasswordtil.setErrorEnabled(false);
                }


                if (mConfirmpasstie.getText().toString().equals("")) {
                    //  Toast.makeText(MainActivity.this, "Please enter Password", Toast.LENGTH_SHORT).show();
                    errorState = true;
                    mConfirmpasstil.setErrorEnabled(true);
                    mConfirmpasstil.setError("*Confirm password is required");
                } else if (mConfirmpasstie.getText().toString().length() < 6) {
                    errorState = true;
                    mConfirmpasstil.setErrorEnabled(true);
                    mConfirmpasstil.setError("Confirm Password can't be small than 6 charactors");
                    //Toast.makeText(MainActivity.this, "Password cant be small than 6 charactors", Toast.LENGTH_SHORT).show();
                } else if (!mConfirmpasstie.getText().toString().equals(mPasswordtie.getText().toString())) {
                    errorState = true;
                    mConfirmpasstil.setErrorEnabled(true);
                    mConfirmpasstil.setError("Confirm pass is not same as password");
                } else {
                    mConfirmpasstil.setError(null);
                    mConfirmpasstil.setErrorEnabled(false);
                }

                if (errorState == false) {
                    Toast.makeText(signupscreen.this, "success", Toast.LENGTH_SHORT).show();

                    Intent HomeScreenActivity = new Intent(signupscreen.this, HomeScreenActivity.class);
                    HomeScreenActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(HomeScreenActivity);
                }
            }
        });
    }


    @Override
    public boolean  onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
            {
                onBackPressed();
                return true;
            }
            case android.R.id.empty: {
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    void findViews() {

        mFirstNametil = findViewById(R.id.signup_firstname_txtinputlayout);
        mFirstNametie= findViewById(R.id.signup_firstname_txtinputedt);
        mLastNametil= findViewById(R.id.signup_Lastname_txtinputlayout);
        mLastNametie = findViewById(R.id.signup_Lastname_txtinputedt);
        mEmailtil = findViewById(R.id.signup_email_txtinputlayout);
        mEmailtie = findViewById(R.id.signup_email_txtinputedt);
        mSignup = findViewById(R.id.signup_signup_btn);
        mPasswordtil = findViewById(R.id.signup_password_txtinputlayout);
        mPasswordtie = findViewById(R.id.signup_password_txtinputedt);
        mConfirmpasstil = findViewById(R.id.signup_confirm_pass_txtinputlayout);
        mConfirmpasstie = findViewById(R.id.signup_confirm_pass_txtinputedt);
    }

}