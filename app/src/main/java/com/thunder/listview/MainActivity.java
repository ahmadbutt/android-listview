package com.thunder.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.thunder.listview.helpers.utils.SharedPreferenceUtil;


public class MainActivity extends AppCompatActivity {

    //View Elements
    TextInputLayout mEmailTIL;
    TextInputLayout mPasswordTIL;
    TextInputEditText mEmailTIET;
    TextInputEditText mPasswordTIET;
    TextView mForgetpass;

    Button mSigninBtn;
    Button mSignup;

    SharedPreferenceUtil mSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSP = new SharedPreferenceUtil(MainActivity.this);


        int isUserLogin = mSP.getLoginType();

        if(isUserLogin == 1) {

            Intent intent = new Intent(MainActivity.this, HomeScreenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            findViews();

            mSignup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent signup = new Intent(MainActivity.this, signupscreen.class);
                    startActivity(signup);

                }
            });

            mSigninBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    boolean errorState = false;

                    if (mEmailTIET.getText().toString().equals("")) {
                        errorState = true;
                        mEmailTIL.setErrorEnabled(true);
                        mEmailTIL.setError("Please enter email");
                        //Toast.makeText(MainActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                    } else if (!Patterns.EMAIL_ADDRESS.matcher(mEmailTIET.getText().toString()).matches()) {
                        //Toast.makeText(MainActivity.this, "Please enter Valid Email", Toast.LENGTH_SHORT).show();
                        errorState = true;
                        mEmailTIL.setErrorEnabled(true);
                        mEmailTIL.setError("Please enter Valid Email");
                    } else {
                        mEmailTIL.setError(null);
                        mEmailTIL.setErrorEnabled(false);
                    }

                    if (mPasswordTIET.getText().toString().equals("")) {
                        //  Toast.makeText(MainActivity.this, "Please enter Password", Toast.LENGTH_SHORT).show();
                        errorState = true;
                        mPasswordTIL.setErrorEnabled(true);
                        mPasswordTIL.setError("*password is required");
                    } else if (mPasswordTIET.getText().toString().length() < 6) {
                        errorState = true;
                        mPasswordTIL.setErrorEnabled(true);
                        mPasswordTIL.setError("Password cant be small than 6 charactors");
                        //Toast.makeText(MainActivity.this, "Password cant be small than 6 charactors", Toast.LENGTH_SHORT).show();
                    } else {
                        mPasswordTIL.setError(null);
                        mPasswordTIL.setErrorEnabled(false);
                    }

                    if (errorState == false) {
//                    Toast.makeText(MainActivity.this, "API CALL", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, HomeScreenActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                }
            });

            mForgetpass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "Forgot Password!", Toast.LENGTH_LONG).show();

                    Intent ForgetPasswordActivty = new Intent(MainActivity.this, ForgetPasswordActivty.class);
                    startActivity(ForgetPasswordActivty);

                }
            });
        }

    }

    void findViews() {
        mEmailTIL = findViewById(R.id.main_email_txt_inp_layout);
        mPasswordTIL = findViewById(R.id.main_password_txt_inp_layout);
        mEmailTIET = findViewById(R.id.main_email_txt_inp_edt);
        mPasswordTIET = findViewById(R.id.main_password_txt_inp_edt);
        mSigninBtn = findViewById(R.id.main_login_btn);
        mSignup = findViewById(R.id.main_signup_btn);
        mForgetpass = findViewById(R.id.main_forgot_password_txt);

    }
}