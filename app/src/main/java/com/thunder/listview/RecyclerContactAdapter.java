package com.thunder.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerContactAdapter extends RecyclerView.Adapter<RecyclerContactAdapter.Viewholder> {
    Context context;
    ArrayList<ContactModel>arrContacts;


    RecyclerContactAdapter(Context context, ArrayList<ContactModel>arrContacts){
    this.context = context;
    this.arrContacts=arrContacts;
        }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
          View view =  LayoutInflater.from(context).inflate(R.layout.contact_row,parent,false);
            Viewholder viewholder =new Viewholder(view);
          return viewholder;

    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
            holder.imgContact.setImageResource(arrContacts.get(position).img);
            holder.txtName.setText(arrContacts.get(position).name);
            holder.txtNumber.setText(arrContacts.get(position).number);
    }

    @Override
    public int getItemCount() {
        return arrContacts.size() ;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView txtName, txtNumber;
        ImageView imgContact;
        public Viewholder(@NonNull View itemView) {
            super(itemView);

            txtName =  itemView.findViewById(R.id.TextName);
            txtNumber = itemView.findViewById(R.id.txtNmbr);
            imgContact = itemView.findViewById(R.id.imgcontact);









        }
    }
}


