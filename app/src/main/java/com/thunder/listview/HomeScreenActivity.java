package com.thunder.listview;

import static com.thunder.listview.R.drawable.ic_launcher_background;
import static com.thunder.listview.R.drawable.ic_launcher_foreground;
import static com.thunder.listview.R.drawable.macdonalds;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.QuickContactBadge;
import android.widget.Toast;

import com.thunder.listview.helpers.utils.AppAlerts;
import com.thunder.listview.helpers.utils.SharedPreferenceUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

public class HomeScreenActivity extends AppCompatActivity {

    RecyclerView recyclerView;
   // private static final String MY_PREFS_NAME = "idName";

    public SharedPreferenceUtil mSP;
  ArrayList<ContactModel> arrConatacts = new ArrayList<>();
  ViewPager viewPager;
  ArrayList<VPModel> arrayList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        viewPager =findViewById(R.id.home_viewpager);
        arrayList.add( new VPModel(macdonalds, "Heading", true) );
        arrayList.add( new VPModel(ic_launcher_foreground, "Heading sdf", false) );
        arrayList.add( new VPModel(ic_launcher_background, "Heading 666", false) );
        arrayList.add( new VPModel(macdonalds, "Heading", true) );
        arrayList.add( new VPModel(macdonalds, "Heading", true) );
        arrayList.add( new VPModel(macdonalds, "Heading", false) );


        MyAdapter myAdapter=new MyAdapter(HomeScreenActivity.this ,arrayList);
        viewPager.setAdapter(myAdapter);
//        viewPager.setCurrentItem();
        recyclerView =findViewById(R.id.RecyclerV);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));



        arrConatacts.add(new ContactModel(ic_launcher_background, "a", "10"));
        arrConatacts.add(new ContactModel(macdonalds, "B", "812938"));
        arrConatacts.add(new ContactModel(ic_launcher_foreground, "C", "834784"));
        arrConatacts.add(new ContactModel(ic_launcher_background, "D", "345345"));
        arrConatacts.add(new ContactModel(macdonalds, "E", "234234234234"));
        arrConatacts.add(new ContactModel(ic_launcher_background, "a", "10"));
        arrConatacts.add(new ContactModel(ic_launcher_foreground, "B", "812938"));
        arrConatacts.add(new ContactModel(ic_launcher_foreground, "C", "834784"));
        arrConatacts.add(new ContactModel(ic_launcher_background, "D", "345345"));
        arrConatacts.add(new ContactModel(macdonalds, "E", "234234234234"));
        arrConatacts.add(new ContactModel(ic_launcher_background, "a", "10"));
        arrConatacts.add(new ContactModel(ic_launcher_foreground, "B", "812938"));
        arrConatacts.add(new ContactModel(ic_launcher_foreground, "C", "834784"));
        arrConatacts.add(new ContactModel(macdonalds, "D", "345345"));
        arrConatacts.add(new ContactModel(ic_launcher_foreground, "E", "234234234234"));

            RecyclerContactAdapter adapter =new RecyclerContactAdapter(this,arrConatacts);
            recyclerView.setAdapter(adapter);

        mSP = new SharedPreferenceUtil(HomeScreenActivity.this);


        mSP.setLoginType(1);


    }

    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an menu file named mymenu.menu which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.home_actionbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {

            case R.id.home_action_logout: {

                AppAlerts appAlerts = new AppAlerts(HomeScreenActivity.this);

                appAlerts.builder.setTitle("Logout")
                        .setMessage("Are you sure you want to Logout?")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation

                                mSP.setLoginType(0);

                                Intent intent = new Intent(HomeScreenActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setCancelable(false);

                appAlerts.Show(getResources().getColor(R.color.appRed));


                return true;
            }

            default: {

            }
        }
        return super.onOptionsItemSelected(item);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragments = new ArrayList<>();
        private final List<String> fragmentTitle = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm)
        {
            super(fm);
        }

        public void add(Fragment fragment, String title)
        {
            fragments.add(fragment);
            fragmentTitle.add(title);
        }

        @Override public Fragment getItem(int position)
        {
            return fragments.get(position);
        }

        @Override public int getCount()
        {
            return fragments.size();
        }


        @Override
        public CharSequence getPageTitle(int position)
        {
            return fragmentTitle.get(position);
        }
    }

}