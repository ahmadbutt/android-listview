package com.thunder.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class ForgetPasswordActivty extends AppCompatActivity {

    TextInputEditText mEmailconfr;
    Button mForgetBtn;
    TextInputLayout mEmailconfrL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_activty);


        mForgetBtn = findViewById(R.id.fahad1);
        mEmailconfr = findViewById(R.id.signup_firstname_txtinputedtFg);
        mEmailconfrL = findViewById(R.id.signup_firstname_txtinputlayoutFg);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("FORGOT PASSWORD");

        mForgetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean errorState = false;


                if (mEmailconfr.getText().toString().equals("")) {
                    errorState = true;
                    mEmailconfrL.setErrorEnabled(true);
                    mEmailconfrL.setError("Please enter email");
                    //Toast.makeText(MainActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(mEmailconfr.getText().toString()).matches()) {
                    //Toast.makeText(MainActivity.this, "Please enter Valid Email", Toast.LENGTH_SHORT).show();
                    errorState = true;
                    mEmailconfrL.setErrorEnabled(true);
                    mEmailconfrL.setError("Please enter Valid Email");
                } else {
                    mEmailconfrL.setError(null);
                    mEmailconfrL.setErrorEnabled(false);

                    Toast.makeText(ForgetPasswordActivty.this, "Forgot password email is sent to your account!", Toast.LENGTH_SHORT).show();

                    onBackPressed();
                }

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Toast.makeText(this, "Activity Ended !", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean  onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
            {
                onBackPressed();
                return true;
            }
            case android.R.id.empty: {
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

}