package com.thunder.listview.helpers.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtil {

    private Context context;
    public SharedPreferences spUser;


    public SharedPreferenceUtil(Context context)
    {
        this.context = context;
        spUser = this.context.getSharedPreferences("sp_user" , Context.MODE_PRIVATE);
    }

    public void setLoginType(int loginType)
    {
        spUser.edit().putInt("Login_status", loginType).commit();
    }

    public int getLoginType()
    {
        return spUser.getInt("Login_status" , 0);
    }

    public void setUserName(String name)
    {
        spUser.edit().putString("UserName", name).commit();
    }

    public String getUserName()
    {
        return spUser.getString("UserName" , "");
    }


}
