package com.thunder.listview.helpers.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;

public class AppAlerts {

    // Data Members
    public AlertDialog alert;
    public AlertDialog.Builder builder;


    // Constructor (Parameterized)
    public AppAlerts(Context context) {
        builder = new AlertDialog.Builder(context);
    }

    public void createAndShowAlert() {

        alert = builder.create();
        alert.show();
    }

    public void Show(int positiveColor) {

        alert = builder.create();
        alert.show();

        Button pButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pButton.setTextColor(positiveColor);
    }
}

